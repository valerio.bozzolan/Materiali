# Introduzione alla lettura

**Se è presente solo un grafico è quello aggregato.**

Partecipanti: **173**

Il sondaggio era aperto ai soci e non ma diffuso solo via mailing list interna.  

![Età di tutti gli iscritti](./etasociiscritti/0.svgz) 

Legenda:

* Contributi attivi sono quelle attività che servono per portare avanti il progetto ovvero sviluppo, segnalazione bug, donazioni, documentazione, traduzioni, community management
* Contributi passivi invece sono quelle di contorno come promozione, utilizzo, discussioni e attività, eventi

## Stato partecipanti

![Stato partecipanti](./completo/1.svgz) 

Abbiamo una divisione abbastanza equa tra soci e non (80/90~), una presenza equa di interessati ad associarsi e non.  
Quelli che non hanno risposto sono stati inseriti tra i non soci.

### Nota
Il sondaggio è stato fatto a cavallo tra aprile e maggio, si è visto una iscrizione di persone che effettivamente erano interessate. Questo è stato possibile grazie alle risposte aperte in cui hanno proprio dato dell'interesse ad aprire una Sezione locale.

# Analisi per tipo di partecipante

## Tempo investito su temi

![](./completo/2.svgz){ width=50% }
![](./completo/3.svgz){ width=50% }
![](./completo/4.svgz){ width=50% }
![](./completo/5.svgz){ width=50% }
![](./completo/6.svgz){ width=50% }

Dal grafico risulta un interesse molto basso nella ricerca del lavoro anche se c'è molto tempo dedicato al lavoro, quindi probabilmente ci si riferisce a alla ricerca di un posto migliore.  
Al tempo stesso il tempo in ambito scolastico è pressochè nullo, probabilmente perché tra i soci e non ci sono molti studenti universitari ad esempio.  
Inoltre c'è molto dedicato alla formazione autonoma e che merita un approfondimento per capire le tematiche.  

### Nota
Un dato da riflettere che non c'è una forte presenza in termini di tempo riguardo l'attivismo o le attività hobbystiche, sembrando quasi un privilegio.

## Coinvolgimento su temi

![](./soci/7.svgz){ width=50% }
![](./nonsoci/7.svgz){ width=50% }

![](./soci/8.svgz){ width=50% }
![](./nonsoci/8.svgz){ width=50% }

![](./soci/9.svgz){ width=50% }
![](./nonsoci/9.svgz){ width=50% }

![](./soci/10.svgz){ width=50% }
![](./nonsoci/10.svgz){ width=50% }

![](./soci/11.svgz){ width=50% }
![](./nonsoci/11.svgz){ width=50% }

![](./soci/12.svgz){ width=50% }
![](./nonsoci/12.svgz){ width=50% }

![](./soci/13.svgz){ width=50% }
![](./nonsoci/13.svgz){ width=50% }

![](./soci/14.svgz){ width=50% }
![](./nonsoci/14.svgz){ width=50% }

![](./soci/15.svgz){ width=50% }
![](./nonsoci/15.svgz){ width=50% }

![](./soci/16.svgz){ width=50% }
![](./nonsoci/16.svgz){ width=50% }

![](./soci/17.svgz){ width=50% }
![](./nonsoci/17.svgz){ width=50% }

Si nota che i soci sono più interessati nella sistemistica che nello sviluppo anche se la differenza non è molta.  
Una parte si occupa di consulenze lato software ma molto pochi lato hardware e Machine Learning, specialmente in ambito embedded. Una presenza quasi a metà riguardo il mondo dell'istruzione e formazione, molto poco riguardo la gestione dei progetti, una bassissima percentuale riguardo gli eventi e comunicazione e pressochè nullo sulle vendite.

### Note

Si può ritenere che abbiamo una forte presenza di sistemisti, un basso interesse lato sviluppo e ancora più alto riguardo hardware. Si nota solo una piccola differenza lato Machine Learning che nei non soci è maggiore.

## Tempo investito per contesto

Il dato non si riferisce alle ore ma era un punteggio da 1 a 5.

![](./soci/18.svgz){ width=50% }
![](./nonsoci/18.svgz){ width=50% }

![](./soci/19.svgz){ width=50% }
![](./nonsoci/19.svgz){ width=50% }

![](./completo/20.svgz){ width=50% }
![](./nonsoci/20.svgz){ width=50% }

![](./soci/21.svgz){ width=50% }
![](./nonsoci/21.svgz){ width=50% }

![](./soci/22.svgz){ width=50% }
![](./nonsoci/22.svgz){ width=50% }

I contributi attivi al FOSS sono molto pochi rispetto ai passivi che sono molto più alti.

### Note

Viene confermato che i contributi sono solo per necessità dirette.

## Adozione al software libero

![](./soci/23.svgz){ width=50% }
![](./nonsoci/23.svgz){ width=50% }

![](./soci/24.svgz){ width=50% }
![](./nonsoci/24.svgz){ width=50% }

![](./soci/25.svgz){ width=50% }
![](./nonsoci/25.svgz){ width=50% }

![](./soci/26.svgz){ width=50% }
![](./nonsoci/26.svgz){ width=50% }

In ambito privato e famiglia l'adozione di soluzioni open source è molto più facile, non nelle scuole e al contrario in ambito lavorativo.

## Stato Servizi Italian Linux Society

![](./soci/27.svgz){ width=50% }
![](./nonsoci/27.svgz){ width=50% }

![](./soci/28.svgz){ width=50% }
![](./nonsoci/28.svgz){ width=50% }

![](./soci/29.svgz){ width=50% }
![](./nonsoci/29.svgz){ width=50% }

![](./soci/30.svgz){ width=50% }
![](./nonsoci/30.svgz){ width=50% }

![](./soci/31.svgz){ width=50% }
![](./nonsoci/31.svgz){ width=50% }

![](./soci/32.svgz){ width=50% }
![](./nonsoci/32.svgz){ width=50% }

![](./soci/33.svgz){ width=50% }
![](./nonsoci/33.svgz){ width=50% }

![](./completo/34.svgz){ width=100% }

![](./soci/35.svgz){ width=50% }
![](./nonsoci/35.svgz){ width=50% }

![](./soci/36.svgz){ width=50% }
![](./nonsoci/36.svgz){ width=50% }

I siti online hanno un gradimento medio, la spedizione adesivi riceve molto successo e un gradimento medio sulla mailing list come anche per il forum (che è stato lanciato appena dopo il sondaggio).  
Il gradimento medio con oscillazioni è presente anche sul Planet, sulle applicazioni per i soci, LugMap e l'assistenza nazionale. L'email riceve un gradimento medio di successo.  
La piattaforma di videocall ha ricevuto votazioni diverse.

## Quali servizi sono strategici

![](./soci/37.svgz){ width=50% }
![](./nonsoci/37.svgz){ width=50% }

![](./soci/38.svgz){ width=50% }
![](./nonsoci/38.svgz){ width=50% }

![](./soci/39.svgz){ width=50% }
![](./nonsoci/39.svgz){ width=50% }

![](./soci/40.svgz){ width=50% }
![](./nonsoci/40.svgz){ width=50% }

![](./soci/41.svgz){ width=50% }
![](./nonsoci/41.svgz){ width=50% }

![](./soci/42.svgz){ width=50% }
![](./nonsoci/42.svgz){ width=50% }

![](./soci/43.svgz){ width=50% }
![](./nonsoci/43.svgz){ width=50% }

![](./soci/44.svgz){ width=50% }
![](./nonsoci/44.svgz){ width=50% }

![](./soci/45.svgz){ width=50% }
![](./nonsoci/45.svgz){ width=50% }

![](./soci/46.svgz){ width=50% }
![](./nonsoci/46.svgz){ width=50% }

La mailing list, la spedizione adesivi, la posta personale riceve votazioni contrastanti.  
I siti internet, il forum, il Planet, i servizi applicativi, LugMap, l'assistenza tecnica nazionale e la piattaforma di videocall ricevono molta importanza.

## Priorità suggerite per ILS

Le opzioni sono state semplificate per migliorare la leggibilità dei grafici:

* favorire partecipazione dei soci (tramite il forum Discourse, incontri periodici, una rubrica online per conoscersi, trovare interessi comuni…)
* favorire interscambio fra gruppi locali (raccogliendo presentazioni, corsi con licenze libere, eventi, video…)
* investire nei progetti (sviluppi, microgrant, bug bounty…)
* migliorare la comunicazione (il modo in cui ILS si approccia al mondo esterno, Visual Identity...)
* migliorare i servizi ai soci (non solo su base volontaria…)

![](./soci/47.svgz){ width=50% }
![](./nonsoci/47.svgz){ width=50% }

![](./soci/48.svgz){ width=50% }
![](./nonsoci/48.svgz){ width=50% }

![](./soci/49.svgz){ width=50% }
![](./nonsoci/49.svgz){ width=50% }

![](./soci/50.svgz){ width=50% }
![](./nonsoci/50.svgz){ width=50% }

![](./soci/51.svgz){ width=50% }
![](./nonsoci/51.svgz){ width=50% }

Viene richiesto di migliorare i servizi ai soci e di investire nello sviluppo di progetti.  
Suggerito invece di migliorare la comunicazione e partecipazione/scambi tra i gruppi locali.

## Provincie

![](./completo/52.svgz){ width=100% }

C'è una forte presenza di Milano, Torino, Pordenone, Brescia, Treviso e Bari.  

## Età media

![](./soci/53.svgz){ width=50% }
![](./nonsoci/53.svgz){ width=50% }

![](./completo/53.svgz){ width=100% }

Metà dei partecipanti ha una età tra i 41 e 52 anni.

# Analisi per età

Sotto i 35 anni di età i partecipanti erano **38** mentre al di sopra **137**.  
I partecipanti che non hanno inserito l'eta sono stati inseriti nel gruppo sotto i 35 anni di età.

## Tempo investito su temi

![](./35sotto/2.svgz){ width=50% }
![](./35sopra/2.svgz){ width=50% }

![](./35sotto/3.svgz){ width=50% }
![](./35sopra/3.svgz){ width=50% }

![](./35sotto/4.svgz){ width=50% }
![](./35sopra/4.svgz){ width=50% }

![](./35sotto/5.svgz){ width=50% }
![](./35sopra/5.svgz){ width=50% }

![](./35sotto/6.svgz){ width=50% }
![](./35sopra/6.svgz){ width=50% }

Sembra che non ci sia una effettiva differenza di investimento di tempo in base all'età.

## Coinvolgimento su temi

![](./35sotto/7.svgz){ width=50% }
![](./35sopra/7.svgz){ width=50% }

![](./35sotto/8.svgz){ width=50% }
![](./35sopra/8.svgz){ width=50% }

![](./35sotto/9.svgz){ width=50% }
![](./35sopra/9.svgz){ width=50% }

![](./35sotto/10.svgz){ width=50% }
![](./35sopra/10.svgz){ width=50% }

![](./35sotto/11.svgz){ width=50% }
![](./35sopra/11.svgz){ width=50% }

![](./35sotto/12.svgz){ width=50% }
![](./35sopra/12.svgz){ width=50% }

![](./35sotto/13.svgz){ width=50% }
![](./35sopra/13.svgz){ width=50% }

![](./35sotto/14.svgz){ width=50% }
![](./35sopra/14.svgz){ width=50% }

![](./35sotto/15.svgz){ width=50% }
![](./35sopra/15.svgz){ width=50% }

![](./35sotto/16.svgz){ width=50% }
![](./35sopra/16.svgz){ width=50% }

![](./35sotto/17.svgz){ width=50% }
![](./35sopra/17.svgz){ width=50% }

La differenza tra le fasce di età si vede nella consulenza che sotto i 35 anni di età è molto più diffusa. Inoltre c'è più interesse sull'hardware ma al tempo stesso meno riguardo la formazione e istruzione. C'è più interesse anche sulla gestione dei progetti ma per il resto sono equivalenti.

## Tempo investito per contesto

![](./35sotto/18.svgz){ width=50% }
![](./35sopra/18.svgz){ width=50% }

![](./35sotto/19.svgz){ width=50% }
![](./35sopra/19.svgz){ width=50% }

![](./completo/20.svgz){ width=50% }
![](./35sopra/20.svgz){ width=50% }

![](./35sotto/21.svgz){ width=50% }
![](./35sopra/21.svgz){ width=50% }

![](./35sotto/22.svgz){ width=50% }
![](./35sopra/22.svgz){ width=50% }

La diffferenza si nota solo in una attenzione maggiore sulla promozione e nel contribuire

## Adozione al software libero

![](./35sotto/23.svgz){ width=50% }
![](./35sopra/23.svgz){ width=50% }

![](./35sotto/24.svgz){ width=50% }
![](./35sopra/24.svgz){ width=50% }

![](./35sotto/25.svgz){ width=50% }
![](./35sopra/25.svgz){ width=50% }

![](./35sotto/26.svgz){ width=50% }
![](./35sopra/26.svgz){ width=50% }

C'è una minore necessità per i giovani di uso di software libero nella propria vita ma in famiglia è diverso.

## Stato Servizi Italian Linux Society

![](./35sotto/27.svgz){ width=50% }
![](./35sopra/27.svgz){ width=50% }

![](./35sotto/28.svgz){ width=50% }
![](./35sopra/28.svgz){ width=50% }

![](./35sotto/29.svgz){ width=50% }
![](./35sopra/29.svgz){ width=50% }

![](./35sotto/30.svgz){ width=50% }
![](./35sopra/30.svgz){ width=50% }

![](./35sotto/31.svgz){ width=50% }
![](./35sopra/31.svgz){ width=50% }

![](./35sotto/32.svgz){ width=50% }
![](./35sopra/32.svgz){ width=50% }

![](./35sotto/33.svgz){ width=50% }
![](./35sopra/33.svgz){ width=50% }

![](./35sotto/34.svgz){ width=50% }
![](./35sopra/34.svgz){ width=50% }

![](./35sotto/35.svgz){ width=50% }
![](./35sopra/35.svgz){ width=50% }

![](./35sotto/36.svgz){ width=50% }
![](./35sopra/36.svgz){ width=50% }

I siti sono considerati buoni da tutti ma di meno sopra i 35 anni.  
Sotti i 35 anni di età la mailing list è considerata sia pessima che non con lo stesso numero di voti.

## Quali servizi sono strategici

![](./35sotto/37.svgz){ width=50% }
![](./35sopra/37.svgz){ width=50% }

![](./35sotto/38.svgz){ width=50% }
![](./35sopra/38.svgz){ width=50% }

![](./35sotto/39.svgz){ width=50% }
![](./35sopra/39.svgz){ width=50% }

![](./35sotto/40.svgz){ width=50% }
![](./35sopra/40.svgz){ width=50% }

![](./35sotto/41.svgz){ width=50% }
![](./35sopra/41.svgz){ width=50% }

![](./35sotto/42.svgz){ width=50% }
![](./35sopra/42.svgz){ width=50% }

![](./35sotto/43.svgz){ width=50% }
![](./35sopra/43.svgz){ width=50% }

![](./35sotto/44.svgz){ width=50% }
![](./35sopra/44.svgz){ width=50% }

![](./35sotto/45.svgz){ width=50% }
![](./35sopra/45.svgz){ width=50% }

![](./35sotto/46.svgz){ width=50% }
![](./35sopra/46.svgz){ width=50% }

## Priorità suggerite per ILS

Le opzioni sono state semplificate per migliorare la leggibilità dei grafici:

* favorire partecipazione dei 35sotto (tramite il forum Discourse, incontri periodici, una rubrica online per conoscersi, trovare interessi comuni…)
* favorire interscambio fra gruppi locali (raccogliendo presentazioni, corsi con licenze libere, eventi, video…)
* investire nei progetti (sviluppi, microgrant, bug bounty…)
* migliorare la comunicazione (il modo in cui ILS si approccia al mondo esterno, Visual Identity...)
* migliorare i servizi ai 35sotto (non solo su base volontaria…)

![](./35sotto/47.svgz){ width=50% }
![](./35sopra/47.svgz){ width=50% }

![](./35sotto/48.svgz){ width=50% }
![](./35sopra/48.svgz){ width=50% }

![](./35sotto/49.svgz){ width=50% }
![](./35sopra/49.svgz){ width=50% }

![](./35sotto/50.svgz){ width=50% }
![](./35sopra/50.svgz){ width=50% }

![](./35sotto/51.svgz){ width=50% }
![](./35sopra/51.svgz){ width=50% }

Entrambi i gruppi considerano investire importate ma i giovani vedono di più la necessità di migliorare i servizi ai soci.

# Domande libere (riassunte e aggregate)

Alcuni punti tra le varie domande sono duplicati quindi non sono stati ripetuti nelle varie domande per semplicità.

## Hai altro da aggiungere per migliorare i servizi Italian Linux Society?

* Strumento per i sondaggi/questionari
* Un utilizzo maggiore del forum a discapito della mailing list, perché le persone devono essere stimolate a partecipare e non considerarlo spam
* Più notizie da parte dell'associazione e promozione
* Fare più networking, collaborazioni e scambio di esperienze all'interno dell'associazione
* Destinare fondi alle associazioni in necessita ma dando priorita ai LUG
* Promuovere di più i servizi ai soci, esternamente per fornire delle alternative sia alle aziende che ad altri
* Più pressioni riguardo l'aderenza del CAD
* Più alfabetizzazione digitale
* Portale per offerte di lavoro
* Indice di esperti linux a livello locale
* Più impegno nella scuola
* Creazione di video di divulgazione, anche in formato breve da 5 minuti ma CC così possono essere usati anche in ambito aziendale o commerciale

## Prova ad aprire la LUGMap nella tua area. Hai qualcosa da segnalare?

* Non conosco i lug menzionati nella mia regione o non sono riuscito a trovarli quando cercavo un lug vicino a me
* https://github.com/Gelma/LugMap/issues/29

## C'è qualcos'altro che vorresti dire ai volontari (direttivo compreso) per essere più efficaci nella promozione del software libero e delle libertà digitali in Italia?

* Far interagire le persone e non solo discuterne sulla ML che è fine a se stesso
* Creare più campagne o framework/tutorial di promozione attività
* Migliorare le abilità comunicative e soft skill dei soci che sono inferiori alla media
* Pagare per una promozione nazionale delle attività associative
* Essere più coinvolti nelle tematiche strategiche del nostro tempo
* Una maggior presenza femminile
* Creare opportunità di incontro con altre realtà
* Cambiare il metodo di comunicazione per essere meno integralisti
* Fare più attenzione al mondo della scuola/università
* Corsi o incontri di livello base
* Un coordinamento per interfacciarsi con le istituzioni o fare lobby insieme ad aziende
* Altre manifestazioni a supporto del FOSS oltre al Linux Day
* Presentare dei casi reali di uso di software open source per le aziende (bigdata, monitoraggio, gestione centralizzata, networking sdn, virtualizzazione, containers, videoconferenza, patch management, sw smartboard, gestione aule)
* Sfruttare di più gli strumenti a norma di legge come FOIA e GDPR per fare campagne mirate
* Portare il software open alle comunità di professionisti che possono realmente imporre linux sul mercato. Es: Avvocati, giudici, notai, politici, agenzie immobiliari e investitori
* Gli incontri di promozione sembrano vecchi e troppo astratti, bisogna creare format nuovi 

## Per il Linux Day 2021, in particolare, quali temi vorresti fossero approfonditi?

* In teoria è un evento per esterni ma non lo è sempre
* Ci sono troppi talk tecnici
* Tematiche incentrate sul cittadino
* Tematiche sul mondo sociale ad esempio di gruppi di volontariato locale
* Si parla di tutto ma non di Linux
* Alfabetizzazione
* Lavoro in remoto
* Linux nelle grandi aziende
* Come funzionano i sistemi sociali nel software libero (i gruppi degli hacker, le motivazioni a contribuire)
* ripercussioni sociali/economiche dell'uso del software libero
* Privacy con l'open source
* Più spazio alle applicazioni
* Introduzione alla programmazione, per esempio con Python
* Promozione nei confronti dei più giovani

## Conclusione

Risulta evidente una differenza generazionale tra i partecipanti dove i più giovani preferiscano strumenti più moderni come il forum rispetto alla mailing list. Riguardo la questione della mailing list per i non soci è ritenuta meno importante, probabilmente perché non sono così fruibili perché utilizzate molto poco al di fuori della mailing list interna ai soci.  
Questa differenza di età si nota tra gli iscritti totali dell'associazione che la fascia 41-64 è il 60%~ degli associati.  
La LugMap è ritenuta più utile dai non soci probabilmente per trovare dei lug nelle vicinanze rispetto a chi fa già parte della associazione, si può studiare un eventuale miglioramento per renderla ancora più fruibile.  
Sempre in ambito di età si nota una differenza in termini di competenze dove i più giovani sono interessati di più allo sviluppo rispetto alle fasce di età più alte. Questo può spiegare anche i problemi di disponibilità nel contribuire ai progetti ILS di sviluppo perché i più giovani sono quelli che effettivamente contribuiscono nel campione con contributi attivi nel mondo FOSS ma sono molti di meno rispetto al resto.
Tutti i servizi offerti sono ritenuti importanti come media ma con ampi margini di miglioramento anche se non è definito di preciso cosa ha bisogno di essere migliorato dalle risposte aperte.   
Risulta evidente che tutti siano d'accordo nell'avere una maggiore azione in ambito comunicativo da parte associativa, però al tempo stesso una buona parte dei soci non è interessata negli eventi.  
I giovani e non soci sentono maggiormente necessaria una interazione maggiore tra i vari LUG per scambio di competenze e materiali.  
La differenza di età molto sproporzionata non offre altri spunti di riflessione se non che i giovani fanno più volontariato oltre a quanto già detto.

### Azioni

* Un redesign dei siti Linux.it e del marchio è in coda da tempo ma visto che i siti sono ritenuti importanti da tutti, specialmente alcuni per chi non fa parte renderli più fruibili può solo giovare.
* Una promozione maggiore da parte dell'associazione è chiesta da tutti ma viene suggerito anche di fare qualcosa per aiutare i soci stessi a farla meglio. Forse si può pensare di proporre dei corsi di marketing o di scambio esperienze da parte dei LUG. Un esempio può essere un blogpost/presentazione ad parte di un LUG su come fanno le cose internamente su certi temi e i problemi che riscontrano.
* Raccogliere il materiale realizzato dai LUG è una idea in coda da tempo ma viste le richieste di interazione tra i LUG potrebbe essere aumentata la priorità. Questo potrebbe portare alla raccolta dei video e fare anche una selezione visto il suggerimenti di farne su varie tematiche.
* Viene richiesta una azione maggiore in ambito scolastico anche perché metà dei partecipanti è coinvolto su queste tematiche. Creare un gruppo interno di persone che segua il tema e che si attivi per fare la differenza potrebbe essere un modo per fare qualcosa di concreto in merito.
* Un analisi sul genere dei soci ma anche dei partecipanti per il prossimo sondaggio è necessario per analizzare anche questo dato.

## Per la prossima edizione

Sarà necessaria una azione maggiore di coinvolgimento degli iscritti ma anche dei partecipanti ai LUG. Abbiamo necessità di avere un campione di partecipanti maggiore per avere più informazioni.  
Anche chiedere quali tipologie di tecnologie si lavora o si usano principalmente per capire le competenze presenti in associazioni.

## Crediti

* Valerio Bozzolan per la prima versione dei grafici su LibreOffice e analisi
* Stefania Delprete per la consulenza lato dati e Python
* Daniele Scasciafratte per lo script, riassunto e analisi

## Licenza 

GPLv3 per il file `genera-pdf.py` mentre il resto è tutto [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.it).

Il sondaggio è stato fatto con [LimeSurvey](https://limesurvey.org/).
